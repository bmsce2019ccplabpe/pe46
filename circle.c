 #include <stdio.h>
#include <math.h>
float get_radius()
{
 float radius;
 printf("enter radius\n");
 scanf("%f",&radius);
 return radius;
 }
 float compute_area(float radius)
 {
   return M_PI*radius*radius;
   }
   float compute_circumference(float radius)
   {
   return 2*M_PI*radius;
   }
   void output_area (float radius, float area)
   {
   printf("the area of circle with radius=%f is %f\n",radius,area);
   }
     void output_circumference(float radius,float circumference)
   {
   printf("circumference of circle with radius=%f is %f\n",radius,circumference);
   }
   
   int main()
   
   {
   float radius,area,circumference;
   radius=get_radius();
   area=compute_area(radius);
   circumference=compute_circumference(radius);
   output_area(radius,area);
   output_circumference(radius,circumference);
   return 0;
   }